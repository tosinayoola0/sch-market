import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { ProfileModule } from '../../profile/profile.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardChartComponent } from '../../dashboard/dashboard-chart/dashboard-chart.component';

@NgModule({
  imports: [
    FormsModule,
    DashboardRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ProfileModule,
    ButtonsModule.forRoot()
  ],
  declarations: [
    DashboardComponent,
    DashboardChartComponent,
  ]
})
export class DashboardModule { }
