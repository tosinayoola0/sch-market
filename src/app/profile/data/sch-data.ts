import { UserDetail } from './../../auth/data/user-detail';

export interface schoolProfile {
  school_name: string,
  school_motto: string,
  school_badge: string,
  extra_curriculum_activities: string,
  school_curriculum: string,
  school_address: string[],
  school_phone_number: number,
  school_email: string,
  school_fees_range: number,
  date_established: Date,
  school_facilities: [string],
  awards_won?: [string]
  school_clubs: [string],
  school_type: string,
  school_state: string,
  school_level: string,
  school_website?: string,
  competitive_advantage: string,
  school_gender: string,
  user: UserDetail


}