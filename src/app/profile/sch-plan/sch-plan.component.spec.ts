import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchPlanComponent } from './sch-plan.component';

describe('SchPlanComponent', () => {
  let component: SchPlanComponent;
  let fixture: ComponentFixture<SchPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchPlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
