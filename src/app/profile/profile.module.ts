import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { DatePipe } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SchPlanComponent } from './sch-plan/sch-plan.component';
import { LogoutComponent } from './logout/logout.component'
import { MiniDashComponent } from './mini-dash/mini-dash.component';
import { PrincipalDetailComponent } from './principal-detail/principal-detail.component';
import { HomeModule } from '../home/home.module';
import { SubscriptionPageComponent } from './subscription-page/subscription-page.component';
import { PhotoUploadComponent } from './photo-upload/photo-upload.component';
import { VideoUploadComponent } from './video-upload/video-upload.component';
import { PaymentFormComponent } from './payment-form/payment-form.component'



const Components = [
  ProfileComponent,
  SchPlanComponent,
  MiniDashComponent,
  LogoutComponent,
  PrincipalDetailComponent,
  SubscriptionPageComponent,
  VideoUploadComponent,
  PhotoUploadComponent,
  PaymentFormComponent,

]

@NgModule({
  declarations: [
    Components,

  ],

  providers: [DatePipe],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ProfileRoutingModule,
    HomeModule,
  ],
  exports: [
    Components
  ]
})
export class ProfileModule { }
