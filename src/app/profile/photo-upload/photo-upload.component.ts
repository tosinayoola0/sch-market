import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../../auth/service/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-photo-upload',
  templateUrl: './photo-upload.component.html',
  styleUrls: ['./photo-upload.component.scss']
})
export class PhotoUploadComponent implements OnInit {
  file: File = null
  imgUrl: string

  constructor(
    private aService: AuthService,
    private route: Router
  ) { }
  imageForm = new FormGroup({
    picture: new FormControl()

  })
  gallery = {
    picture: ''
  }

  ngOnInit(): void {
  }

  onChange(event) {
    let file
    if (event.target.files.length > 0) {
      file = event.target.files[0]
      this.imageForm.get('picture').setValue(file)
    }
    this.imageForm.get('picture').updateValueAndValidity()

    const reader = new FileReader()
    reader.onload = () => {
      this.imgUrl = reader.result as string
    }
    reader.readAsDataURL(file)
  }
  createGallery() {
    let formData = new FormData
    for (let [key, formControl] of Object.entries(this.imageForm.controls)) {
      formData.append(key, formControl.value)
    }
    formData.append('gallery', this.imageForm.value)
    this.aService.uploadPhotos(formData).subscribe((res) => {
      this.aService.resultHandler(res)
    },
      err => this.aService.errorHandler(err)
    )
  }

}
