import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { schoolProfile } from '../data/sch-data'
import { AuthService } from '../../auth/service/auth.service'
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  profile: schoolProfile[]
  userProfile
  fees_range = [
    '₦10, 000.00 - ₦100, 000.00',
    '₦101, 000.00 - ₦200, 000.00',
    '₦201, 000.00 - ₦300, 000.00',
    '₦301, 000.00 - ₦400, 000.00',
    '₦401, 000.00 - ₦500, 000.00 ',
    '₦501, 000.00 - ₦600, 000.00',
    '₦601, 000.00 - ₦700, 000.00',
    '₦701, 000.00 - ₦800, 000.00',
    '₦801, 000.00 - ₦900, 000.00',
    '₦901, 000.00 - ₦1, 000, 000.00',
  ]
  id;
  constructor(
    private authService: AuthService,
    public datePipe: DatePipe,
    private route: ActivatedRoute,
    private router: Router
  ) {

  }

  ngOnInit(): void {
    this.id = localStorage.getItem('id')

    this.authService.getSchoolProfile(this.id).subscribe((data: schoolProfile) => {

      this.userProfile = data
      let school_name = data.school_name
      console.log(school_name)
      this.profileForm = new FormGroup({
        school_name: new FormControl(data ? data?.school_name : [Validators.required]),
        school_address: new FormControl(data ? data.school_address : [Validators.required]),
        school_badge: new FormControl(data ? data.school_badge : [Validators.required]),
        school_email: new FormControl(data ? data.school_email : [Validators.required]),
        school_phone_number: new FormControl(data ? data.school_phone_number : [Validators.required]),
        school_curriculum: new FormControl(data ? data.school_curriculum : [Validators.required]),
        extra_curriculum_activities: new FormControl(data ? data.extra_curriculum_activities : [Validators.required]),
        school_motto: new FormControl(data ? data.school_motto : [Validators.required]),
        date_established: new FormControl(data ? data.date_established : [Validators.required]),
        school_facilities: new FormControl(data ? data.school_facilities : [Validators.required]),
        awards_won: new FormControl(data ? data?.awards_won : [Validators.required]),
        competitive_advantage: new FormControl(data ? data?.competitive_advantage : [Validators.required]),
        school_clubs: new FormControl(data ? data.school_clubs : [Validators.required]),
        school_type: new FormControl(data ? data.school_type : [Validators.required]),
        school_state: new FormControl(data ? data.school_state : [Validators.required]),
        school_level: new FormControl(data ? data.school_level : [Validators.required]),
        school_website: new FormControl(data ? data.school_website : [Validators.required]),
        school_gender: new FormControl(data ? data.school_gender : [Validators.required]),
        school_fees_range: new FormControl(data ? data.school_fees_range : [Validators.required]),
      })

    })


  }
  onChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0]
      this.profileForm.get('school_badge').setValue(file)
    }
  }

  createProfile() {
    console.log('files')
    console.log(this.profileForm)
    let formData: any = new FormData
    for (const [key, formControl] of Object.entries(this.profileForm.controls)) {

      formData.append(key, formControl.value)
    }
    this.profileForm.controls['date_established'].setValue(
      this.datePipe.transform(this.profileForm.value['date_established'], 'MM-dd-yyyy')
    )
    console.log(this.profileForm.value['date_established'])
    formData.append('school_profile', this.profileForm.value)
    if (this.userProfile.school_name !== '') {
      this.authService.upateSchoolProfile(this.id, formData).subscribe(
        (res) => {
          this.authService.resultHandler(res)
          this.router.navigate(['/school-dashboard'])
        },
        (err) => this.authService.errorHandler(err)
      )
    }
    else {
      this.authService.postSchoolProfile(formData).subscribe(
        res => {
          schoolProfile => this.profile.push()
          this.authService.resultHandler(res)
          console.log('school profile created successful')
          this.router.navigate(['/school-dashboard'])
        },
        err => {
          this.authService.errorHandler(err)
        }
      )
    }


  }

  modifyDate(date) {
    let strignified = JSON.stringify(date.value)
    let newDate = strignified.substring(1, 11)
    this.profileForm.controls['date_established'].setValue(newDate)
  }



  /*
  onChange(event){
    if(event.target.files.length > 0){
      const file = event.target.files[0]
      this.profileForm.get('badge').setValue(file)
    }
  }
  */
  /*
     $(() =>{
       $('date_established').datepicker({
         changeMonth: true,
         changeYear: true,
         yearRange:'1890-2030',
         onSelect: dateText =>{ this.profileForm.setValue('school')}
       })
     })
   */

}

