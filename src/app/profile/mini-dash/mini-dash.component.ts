import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/service/auth.service';

@Component({
  selector: 'app-mini-dash',
  templateUrl: './mini-dash.component.html',
  styleUrls: ['./mini-dash.component.scss']
})
export class MiniDashComponent implements OnInit {
  id: any;
  profile: any;
  constructor(private authService: AuthService) {

  }

  ngOnInit(): void {
    this.displayProfile()
  }

  displayProfile() {
    const id = localStorage.getItem('id')
    this.authService.getSchoolProfile(id).subscribe((res: any) => {
      if (res) {
        if (res) {
          this.profile = res
          localStorage.setItem('profile', JSON.stringify(res))
        }
      }
    },
      (err) => console.log(err)

    )
  }

}
