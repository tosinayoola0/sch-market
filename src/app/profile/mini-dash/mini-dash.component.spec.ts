import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniDashComponent } from './mini-dash.component';

describe('MiniDashComponent', () => {
  let component: MiniDashComponent;
  let fixture: ComponentFixture<MiniDashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiniDashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MiniDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
