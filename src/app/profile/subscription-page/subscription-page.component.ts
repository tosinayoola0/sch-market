import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subscription-page',
  templateUrl: './subscription-page.component.html',
  styleUrls: ['./subscription-page.component.scss']
})
export class SubscriptionPageComponent implements OnInit {
  badge;
  school_name;
  constructor() { }

  ngOnInit(): void {
    this.school_name = JSON.parse(localStorage.getItem('profile')).school_name
    this.badge = JSON.parse(localStorage.getItem('profile')).school_badge
    console.log(this.school_name)
    console.log(this.badge)
  }

}
