import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogoutComponent } from './logout/logout.component';
import { ProfileComponent } from './profile/profile.component'
import { PrincipalDetailComponent } from './principal-detail/principal-detail.component';
import { SubscriptionPageComponent } from './subscription-page/subscription-page.component';
import { PhotoUploadComponent } from './photo-upload/photo-upload.component';
import { VideoUploadComponent } from './video-upload/video-upload.component'
import { CanActivateService } from '../auth/can-activate.service';
import { PaymentFormComponent } from './payment-form/payment-form.component';


const routes: Routes = [
  { path: 'profile', component: ProfileComponent, canActivate: [CanActivateService] },
  { path: 'logout', component: LogoutComponent },
  { path: 'principal-detail', component: PrincipalDetailComponent, canActivate: [CanActivateService] },
  { path: 'subscription-page', component: SubscriptionPageComponent, canActivate: [CanActivateService] },
  { path: 'sch-gallery', component: PhotoUploadComponent, canActivate: [CanActivateService] },
  { path: 'upload-video', component: VideoUploadComponent, canActivate: [CanActivateService] },
  { path: 'payment-page', component: PaymentFormComponent, canActivate: [CanActivateService] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
