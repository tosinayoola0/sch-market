import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../auth/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-video-upload',
  templateUrl: './video-upload.component.html',
  styleUrls: ['./video-upload.component.scss']
})
export class VideoUploadComponent implements OnInit {

  url: string
  videoUploadForm = new FormGroup({
    intro_video: new FormControl(['', Validators.required])
  })

  videUpload = {
    intro_video: ''
  }
  constructor(
    private aService: AuthService,
    private router: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  onChange(event) {
    let file
    if (event.target.files.length > 0) {
      file = event.target.files[0]
      this.videoUploadForm.get('intro_video').setValue(file);
    }
    this.videoUploadForm.get('intro_video').updateValueAndValidity()

    const reader = new FileReader()
    reader.onload = () => {
      this.url = reader.result as string
    }
    reader.readAsDataURL(file)
  }

  uploadVideo() {
    let formData = new FormData()
    for (let [key, formControl] of Object.entries(this.videoUploadForm.controls)) {
      formData.append(key, formControl.value)
    }
    formData.append('video', this.videoUploadForm.value)
    this.aService.uploadVideos(formData).subscribe(
      (res: any) => this.aService.resultHandler(res.message),
      err => this.aService.errorHandler(err)
    )
  }

}
