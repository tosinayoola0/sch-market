import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/service/auth.service'


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(
    public aService: AuthService
  ) { }

  ngOnInit(): void {
    this.aService.userLogout()
    console.log('user logoiut')
  }


}
