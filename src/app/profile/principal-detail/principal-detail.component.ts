import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../auth/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-principal-detail',
  templateUrl: './principal-detail.component.html',
  styleUrls: ['./principal-detail.component.scss']
})
export class PrincipalDetailComponent implements OnInit {
  principalDetailForm: FormGroup;
  principalData;
  id;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.id = localStorage.getItem('id')
    this.authService.DisplayPrincipalDetail(this.id).subscribe((res: any) => {
      this.principalData = res.principal_detail
      this.principalDetailForm = new FormGroup({
        principal_name: new FormControl(res ? res?.principal_detail.principal_name : Validators.required),
        display_image: new FormControl(res ? res?.principal_detail.display_image : Validators.required),
        post_held: new FormControl(res ? res?.principal_detail.post_held : Validators.required),
        phone_number: new FormControl(res ? res?.principal_detail.phone_number : Validators.required)
      })
    }
    )
  }
  onChange(event) {
    console.log(this.principalData)
    if (event.target.files.length > 0) {
      let file = event.target.files[0]
      this.principalDetailForm.get('display_image').setValue(file)
    }

  }

  onSubmit() {
    let formData = new FormData
    for (const [key, formControl] of Object.entries(this.principalDetailForm.controls)) {
      formData.append(key, formControl.value)
    }
    formData.append('principal_detail', this.principalDetailForm.value)
    if (this.principalData.principal_name === '') {
      this.authService.createPrincipalProfile(formData).subscribe(
        (res) => {
          this.authService.resultHandler(res)
          this.router.navigate(['/dashboard'])
        },
        (err) => {
          this.authService.errorHandler(err)
        }
      )
    }
    else {
      this.authService.updatePrincipalDetail(this.id, formData).subscribe(
        (res) => {
          console.log(res)
          this.authService.resultHandler(res)
          this.router.navigate(['/dashboard'])
        },
        (err) => {
          this.authService.errorHandler(err)
          console.log(err)
        }
      )
    }
  }

}
