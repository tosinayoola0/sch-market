
import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import {ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms'

@Component({
  selector: 'app-img-upload',
  templateUrl: './img-upload.component.html',
  styleUrls: ['./img-upload.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ImgUploadComponent,
      multi: true
    }
  ]
})


export class ImgUploadComponent implements ControlValueAccessor {
  onChange: Function
  private file: File | null = null;

  @HostListener('change', ['$event.target.files']) emitFiles(event: FileList){
    const file = event && event.item(0);
    this.file = file
  }
  constructor(private host: ElementRef<HTMLInputElement>) { }

  ngOnInit(): void {
  }

  writeValue(value: null){
    this.host.nativeElement.value = ''
    this.file = null
  }
  registerOnChange(Fn: Function){
    this.onChange = Fn
  }
  registerOnTouched(Fn: Function){}

}
