import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { FormControl, FormGroup, Validator, Validators } from '@angular/forms';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.scss']
})
export class PaymentFormComponent implements OnInit {
  paymentForm = new FormGroup({
    name: new FormControl(['', Validators.required]),
    card_number: new FormControl(['', Validators.required]),
    cvc: new FormControl(['', Validators.required]),
    expiry_date: new FormControl(['', Validators.required])
  })

  formInstance = {
    name: '',
    card_number: '',
    cvc: '',
    expiry_date: ''
  }
  constructor(
    private profileService: ProfileService
  ) { }


  ngOnInit(): void {
  }

  onSubmit() {
    this.profileService.makePayment(this.paymentForm.value).subscribe(
      res => {
        console.log('payment successful'),
          console.log(this.paymentForm.value)
      }
    )
  }

}
