import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import * as Rx from 'rxjs'
import { Blog } from './blog';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth/service/auth.service';


@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  News: Blog[];
  news = new Rx.BehaviorSubject([])
  location = new Rx.BehaviorSubject<any>({})
  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) { }

  extraInfo(res: any) {
    return res.articles || []
  }

  getNews(): Observable<Blog[]> {
    const url = `${environment.baseUrl}language=en&apiKey=${environment.key}`
    return this.httpClient.get<Blog[]>(url).pipe(map(this.extraInfo))
  }

  getPostion(position) {
    const key = '3aa174eb23334f06b5a9607db4c05d29'
    const lat = position.coords.latitude;
    const long = position.coords.longitude
    console.log(lat, long)
    const url = `https://api.opencagedata.com/geocode/v1/json?q=${lat}+${long}&key=${key}`
    return this.httpClient.get(url).subscribe(
      (res: any) => {
        this.location.next(res)
      },
      err => console.log(err)
    )
  }

  errorHandler(err) {
    let msg = ''
    if (err.code === 1) {
      msg = 'Access Denied'
    }
    else if (err.code === 2) {
      msg = 'POSITION_UNAVAILABLE'
    }
    else if (err.code === 3) {
      msg = 'TIMEOUT'
    }
    return this.authService.errorHandler(msg)
  }

  getLocation() {
    const location = navigator.geolocation
    return location.getCurrentPosition((position) => {
      const key = '3aa174eb23334f06b5a9607db4c05d29'
      const lat = position.coords.latitude;
      const long = position.coords.longitude
      console.log(lat, long)
      const url = `https://api.opencagedata.com/geocode/v1/json?q=${lat}+${long}&key=${key}`
      return this.httpClient.get(url).subscribe(
        (res: any) => {
          this.location.next(res)
        },
        err => console.log(err)
      )
    }, this.errorHandler, { maximumAge: 75000, enableHighAccuracy: true })
  }


  /*
  getNews(){
    const url = `${environment.baseUrl}language=en&apiKey=${environment.key}`
    this.httpClient.get(url),
    (err: Error)=>{
      console.log(err)
    }
    
  }
  */

}
