export interface Blog {
  title: string,
  images :string,
  content: string
}
