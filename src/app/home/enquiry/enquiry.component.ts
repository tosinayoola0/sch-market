import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SchoolsService } from '../schools.service';
import { AuthService } from '../../auth/service/auth.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.scss']
})
export class EnquiryComponent implements OnInit {
  profile;
  id;
  enquiryForm;

  /**
   * enquiry = {
    recipient_email: '',
    subject: '',
    message: '',
    contact_email: ''
  }
   */


  subject = [
    'Cost of Tuition Fees',
    'Addmission Form',
    'Request For Direcction',
    'Request A Tour Of School',
    'Others',
  ]
  constructor(
    private schService: SchoolsService,
    private aRoute: ActivatedRoute,
    private aService: AuthService,
  ) {
    this.id = this.aRoute.snapshot.paramMap.get('id');
    this.schService.getSchoolById(this.id)
  }

  ngOnInit(): void {
    this.enquiryForm = new FormGroup({
      'subject': new FormControl('', Validators.required),
      'body': new FormControl('', Validators.required),
      'contact_email': new FormControl('', Validators.email),
      'recipient_email': new FormControl(Validators.required, Validators.email),

    });

    this.schService.profile.subscribe((res) => {
      this.profile = res
    }
    )

  }

  onSubmit() {
    this.enquiryForm.controls['recipient_email'].setValue(this.profile.school_email)
    console.log(this.enquiryForm.value)
    this.schService.sendEnquiry(this.enquiryForm.value).subscribe(
      (res) => {
        console.log(res)
        this.aService.resultHandler(res)
      },
      (err) => {
        this.aService.errorHandler(err)
        console.log(err)
      }
    )

  }

}
