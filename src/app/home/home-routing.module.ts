import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component'
import { ReviewComponent } from './review/review.component';
import { SchDetailComponent } from './sch-detail/sch-detail.component';
import { GalleryComponent } from './gallery/gallery.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'school-detail/:id', component: SchDetailComponent },
  { path: 'school-detail/:id/school-reviews', component: ReviewComponent },
  { path: 'school-detail/:id/school-gallery', component: GalleryComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
