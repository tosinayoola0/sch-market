import { Component, OnInit } from '@angular/core';
import { SchoolsService } from '../schools.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.scss']
})
export class ReviewListComponent implements OnInit {
  reviews = []
  id;
  constructor(
    private schService: SchoolsService,
    private activatedRoute: ActivatedRoute
  ) {
    this.id = +this.activatedRoute.snapshot.paramMap.get('id')
    this.schService.getReviews(this.id)
  }

  ngOnInit(): void {
    this.schService.reviews.subscribe(
      (res: any) => {
        this.reviews = res.review
        console.log(this.reviews)
      },
      (err) => {
        console.log(err)
      }
    )

  }

  goBack() {
    window.history.back
  }

}
