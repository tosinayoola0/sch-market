import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SchoolsService } from '../schools.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.scss']
})
export class AddReviewComponent implements OnInit {
  starRating = 0;
  reviewForm = new FormGroup({
    'name': new FormControl('', Validators.required),
    'review': new FormControl('', Validators.required),
    'rating': new FormControl('', Validators.required),

  })

  review: {
    'name': '',
    'review': ''
    'rating': '',

  }
  constructor(
    private schoolService: SchoolsService,
    private activatedRoute: ActivatedRoute

  ) { }

  ngOnInit(): void {
    console.log(localStorage.getItem('id'))
  }


  onSubmit(form: any) {
    console.log(form)
    this.schoolService.addReview(form)
  }

}
