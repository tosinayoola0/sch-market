import { Component, ElementRef, ErrorHandler, OnInit, ViewChild } from '@angular/core';
import { AppServiceService } from '../../shared/app-service.service';
import { SchoolsService } from '../schools.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { fromEvent, Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { startWith, map, filter, debounce, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { AuthService } from '../../auth/service/auth.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  @ViewChild('searchField', { static: true }) searchField: ElementRef;
  public url = `${environment.mainUrl}/school-profile/search-school`
  response: Observable<any[]>
  isSearching: boolean
  newSearchField = new FormControl()
  public params = new HttpParams({
    fromObject: {
      action: 'open Search',
      format: 'json',
      origin: '*'
    }
  })

  constructor(
    private authService: AuthService,
    private appService: AppServiceService,
    private schoolService: SchoolsService
  ) {
    this.isSearching = true
    //this.response = []
    this.appService.location.subscribe((data) => {
    })
  }

  seachForm
  ngOnInit(): void {
    this.appService.getLocation()

    //fromEvent(this.searchField.nativeElement, 'keyup').pipe(
    fromEvent(this.newSearchField.value, 'keyup').pipe(
      map((event: any) => {
        return event.target.value
      }),
      filter(res => res.length > 2),
      debounceTime(1000),
      distinctUntilChanged()
    ).subscribe((txt: string) => {
      this.isSearching = true
      this.schoolSearch(txt).subscribe((res: any) => {
        console.log(res.school_name)
        this.isSearching = false;
        this.response = res;
        console.log(this.response)
        this.authService.resultHandler(res)
      }), (err) => {
        this.isSearching = false
        this.authService.errorHandler(err)
      }
    }

    )
  }

  schoolSearch(form: any) {
    console.log(form)
    if (form == '') {
      return of([])
    }
    return this.schoolService.searchSchool(form)
  }


}

