import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../auth/service/auth.service';
import * as Rx from 'rxjs'
import { schoolProfile } from '../profile/data/sch-data';
import { environment } from '../../environments/environment'
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class SchoolsService {
  school_list = new Rx.BehaviorSubject<any>({})
  id
  reviews = new Rx.BehaviorSubject<any>({})
  profile = new Rx.BehaviorSubject<any>({})
  extraInfo = new Rx.BehaviorSubject<any>({})
  PARAMS = new HttpParams({
    fromObject: {
      action: 'opensearch',
      format: 'json',
      origin: '*'
    }
  })
  constructor(
    private httpClient: HttpClient,
    private authService: AuthService,
    private router: Router
  ) { }

  displaySchoolList() {
    const url = `${environment.mainUrl}/school-profile/list`
    return this.httpClient.get(url).subscribe((res: schoolProfile) => {
      if (res) {
        this.school_list.next(res)
      }
    },
      (err) => console.log(err)
    )
  }

  getSchoolById(id) {
    const url = `${environment.mainUrl}/school-profile/detail/${id}`
    return this.httpClient.get(url).subscribe((data: schoolProfile) => {
      if (data) {
        this.profile.next(data)
      }

    },
      err => console.log(err)
    )

  }


  sendEnquiry(form: any) {
    const url = `${environment.mainUrl}/school-detail/send-mail`
    return this.httpClient.post(url, form)
  }

  addReview(form: any) {
    const id = localStorage.getItem('id')
    console.log(id)
    const url = `${environment.mainUrl}/school-detail/${id}/create-review`
    return this.httpClient.post(url, form).subscribe(
      (res) => {
        this.authService.resultHandler(res)
        this.router.navigate([`/school-detail/${id}/school-reviews`])
      },
      (err) => {
        console.log(err)
        this.authService.errorHandler(err)
      }
    )
  }

  getReviews(id: any) {
    const url = `${environment.mainUrl}/school-profile/detail/${id}`
    return this.httpClient.get(url).subscribe(
      (res) => {
        this.reviews.next(res)
        console.log(res)
      },
      (err) => console.log(err)
    )
  }

  searchSchool(form: any) {
    const url = `${environment.mainUrl}/school-profile/school-search?search=${form}`
    return this.httpClient.get(url)
  }

  /**
   * 
   *  searchSchool(txt: string) {
    if (txt === '') {
      return ([])
    }
    const url = `${environment.mainUrl}/school-profile/search-school`
    return this.httpClient.get<[any, string[]]>(url, { params: this.PARAMS.set('search', txt) })
      .pipe(
        startWith(''),
        map((response) => Response[1])
      )

  }
   */

}
