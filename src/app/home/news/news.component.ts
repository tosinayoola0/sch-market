import { AppServiceService } from './../../shared/app-service.service';
import { Blog } from './../../shared/blog';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  news: Blog[]
  constructor(
    public appServices: AppServiceService,
  ) { }

  ngOnInit(): void {
    this.getNews();
  }

  getNews() {
    this.appServices.getNews().subscribe(
      news => this.news = news
    )
  }

}
