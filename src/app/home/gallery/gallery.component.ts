import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  school;
  constructor(private route: Router) { }

  ngOnInit(): void {
    this.school = JSON.parse(localStorage.getItem('profile'))
  }

  routeBy() {
    return this.route.navigateByUrl('school-detail/:id/school-gallery')
  }
}
