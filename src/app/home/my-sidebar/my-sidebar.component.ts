import { AfterViewChecked, Component, ElementRef, Renderer2, OnInit, ViewChild } from '@angular/core';
import { CollapseDirective } from 'ngx-bootstrap/collapse';

@Component({
  selector: 'app-my-sidebar',
  templateUrl: './my-sidebar.component.html',
  styleUrls: ['./my-sidebar.component.scss']
})
export class MySidebarComponent implements OnInit, AfterViewChecked {

  private _isCollapse: boolean = true
  set isCollapse(value) {
    this._isCollapse = value
  }
  get isCollapse() {
    if (this.collapseRef) {
      if (getComputedStyle(this.collapseRef.nativeElement).getPropertyValue('display') === 'flex') {
        this.renderer.removeStyle(this.collapseRef.nativeElement, 'overflow');
      }
    }
    return this._isCollapse
  }
  @ViewChild(CollapseDirective, { read: ElementRef, static: false }) collapse: CollapseDirective
  collapseRef

  constructor(
    private renderer: Renderer2
  ) { }

  ngOnInit(): void {
  }

  status: boolean = false;
  clickEvent() {
    this.status = !this.status;
  }

  ngAfterViewChecked(): void {
    this.collapseRef = this.collapse
  }
}
