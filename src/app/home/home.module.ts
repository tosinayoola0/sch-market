import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { BodyComponent } from './body/body.component';
import { NewsComponent } from './news/news.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module'
import { NewsListComponent } from './news-list/news-list.component';
import { SchoolListComponent } from './school-list/school-list.component'
import { SchDetailComponent } from './sch-detail/sch-detail.component'
import { MyFooterComponent } from './my-footer/my-footer.component';
import { ReviewComponent } from './review/review.component';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { AddReviewComponent } from './add-review/add-review.component';
import { ReviewListComponent } from './review-list/review-list.component';
import { MySidebarComponent } from './my-sidebar/my-sidebar.component';
import { GalleryComponent } from './gallery/gallery.component';
import { NgxTypeaheadModule } from 'ngx-typeahead';
import { NgSelectModule } from '@ng-select/ng-select';

const Components = [
  NavbarComponent,
  IndexComponent,
  BodyComponent,
  NewsComponent,
  NewsListComponent,
  SchoolListComponent,
  SchDetailComponent,
  MyFooterComponent,
  ReviewComponent,
  EnquiryComponent,
  AddReviewComponent,
  ReviewListComponent,
  MySidebarComponent,
  GalleryComponent

]

@NgModule({
  declarations: [Components],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    HomeRoutingModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxTypeaheadModule,
    NgSelectModule,
  ],
  exports: [Components]
})
export class HomeModule { }
