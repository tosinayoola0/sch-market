import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/service/auth.service';
import { SchoolsService } from '../schools.service';
import { schoolProfile } from '../../profile/data/sch-data';
@Component({
  selector: 'app-school-list',
  templateUrl: './school-list.component.html',
  styleUrls: ['./school-list.component.scss']
})
export class SchoolListComponent implements OnInit {
  schoolsProfile: [schoolProfile]
  constructor(private schoolService: SchoolsService) {
    this.schoolService.displaySchoolList()
  }

  ngOnInit(): void {
    this.schoolService.school_list.subscribe((data: any) => {
      if (data.school_name !== ' ') {
        this.schoolsProfile = data
      }

    })

  }


}
