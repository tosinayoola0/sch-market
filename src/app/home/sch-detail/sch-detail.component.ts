import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth/service/auth.service';
import { schoolProfile } from '../../profile/data/sch-data';
import { DatePipe } from '@angular/common';
import { SchoolsService } from '../schools.service';

@Component({
  selector: 'app-sch-detail',
  templateUrl: './sch-detail.component.html',
  styleUrls: ['./sch-detail.component.scss']
})
export class SchDetailComponent implements OnInit {
  id;
  gallery
  profile;
  date_established;
  extraData;
  constructor(
    private authService: AuthService,
    private schService: SchoolsService,
    private route: ActivatedRoute,
    private datePipe: DatePipe
  ) {

    this.id = this.route.snapshot.paramMap.get('id')
    this.schService.getSchoolById(this.id)
    localStorage.setItem('id', this.id)

  }

  ngOnInit(): void {

    this.schService.profile.subscribe((data: any) => {
      if (data) {
        this.profile = data
        localStorage.setItem('profile', JSON.stringify(data))
        this.date_established = this.datePipe.transform(this.profile['date_established'], 'MMMM, YYYY')
      }
    },
      err => console.log(err)
    )

    /**
     * this.schService.extraInfo.subscribe((data) => {
      if (data) {
        this.extraData = data
        console.log(this.extraData)
      }
    },
      err => console.log(err))
    
    }
    */
  }







}
