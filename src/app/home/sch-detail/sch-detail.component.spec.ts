import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchDetailComponent } from './sch-detail.component';

describe('SchDetailComponent', () => {
  let component: SchDetailComponent;
  let fixture: ComponentFixture<SchDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
