import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  constructor(private authService: AuthService) { }

  restPasswordForm = new FormGroup({
    email: new FormControl(['', Validators.email])
  })
  resetPassword: {
    email: ''
  }
  ngOnInit(): void {

  }

  onSubbmit(form) {
    this.authService.resetPassword(form).subscribe(
      (res) => {
        this.authService.resultHandler(res)
        console.log(form)
      },
      (err) => {
        console.log(err)
        this.authService.errorHandler(err)
      }
    )
  }

}
