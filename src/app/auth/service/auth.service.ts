import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as Rx from 'rxjs';
import { catchError, map, shareReplay } from 'rxjs/operators'
import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import { FormBuilder, FormGroup } from '@angular/forms'
import { UserDetail } from '../data/user-detail';
import { Login } from '../data/login'
import { schoolProfile } from '../../profile/data/sch-data';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { env } from 'process';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  mainUrl = environment.mainUrl
  userToken
  uidb64: any;
  token: any;

  public user: Observable<UserDetail>;
  profile = new Rx.BehaviorSubject([])
  public schoolProfile: Observable<schoolProfile>
  schProfile = new Rx.BehaviorSubject([])
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'Application/Json',
    })
  }

  imgOptions = {
    headers: new HttpHeaders({
    })
  }


  headers = new HttpHeaders().set('Content-Type', 'application/json')
  private loginUser: BehaviorSubject<Login>;
  private currentUser: BehaviorSubject<UserDetail>;

  constructor(
    private httpClient: HttpClient,
    private route: Router,
    private session: ToastrService
  ) {
    this.currentUser = new BehaviorSubject<UserDetail>(JSON.parse(localStorage.getItem('user')))
    this.user = this.currentUser.asObservable();
  }

  public get uservalue(): Login {
    return this.loginUser.value
  }

  userSignup(user: UserDetail) {
    const url = `${this.mainUrl}/account/registration`;
    return this.httpClient.post(url, user)
  }



  getToken() {
    return localStorage.getItem('token')
  }
  get IsLoggedIn(): boolean {
    const token = localStorage.getItem('token')
    return (token !== null) ? true : false
  }

  login(login: Login) {
    const url = `${this.mainUrl}/account/login`
    return this.httpClient.post<any>(url, login).subscribe(
      (res: any) => {
        //console.log(res)
        this.userToken = res.token
        localStorage.setItem('token', res.token)
        this.route.navigate(['/school-dashboard'])
        this.resultHandler(res)
        //console.log(localStorage.getItem('token'))
        localStorage.setItem('id', res.id)
      },
      (err) => {
        console.log(err)
        this.errorHandler(err)
      }
    )
  }

  userLogout() {
    const remove_token = localStorage.removeItem('token')
    remove_token
    console.log('logged out')
    this.route.navigate(['/account/login'])
    localStorage.clear()
  }



  postSchoolProfile(profile: schoolProfile): Observable<schoolProfile> {
    const url = `${environment.mainUrl}/school-profile/create`
    return this.httpClient.post<schoolProfile>(url, profile, {
      headers: new HttpHeaders({})
    })
  }



  getSchoolProfile(id) {
    const url = `${environment.mainUrl}/school-profile/detail/${id}`
    return this.httpClient.get(url)

  }


  upateSchoolProfile(id: number, profile: schoolProfile) {
    const url = `${environment.mainUrl}/school-profile/update/${id}`
    return this.httpClient.put(url, profile, {
      headers: new HttpHeaders({})
    })
  }

  resultHandler = (data) => {
    this.session.success(data.message, data.label || "", {
      timeOut: 3000
    })
  }

  errorHandler = (data) => {
    const err = data
    typeof data.error === 'object' ? data.error.message : data.statusText;
    this.session.error(err.label.message || "Error")
  }

  createPrincipalProfile(form: any) {
    const url = `${environment.mainUrl}/school-detail/create-principal-detail`
    return this.httpClient.post(url, form)
  }

  updatePrincipalDetail(id, form) {
    const url = `${environment.mainUrl}/school-detail/update-prinicipal-detail/${id}`
    return this.httpClient.put(id, form)
  }

  DisplayPrincipalDetail(id) {
    const url = `${environment.mainUrl}/school-profile/detail/${id}`
    return this.httpClient.get(url)
  }

  changePassword(id, form) {
    const url = `${environment.mainUrl}/account/${id}/change-password`
    return this.httpClient.put(url, form)
  }

  getPasswordToken(token, uidb64) {
    const url = `${environment.mainUrl}/account/password-reset/${uidb64}/${token}`
    return this.httpClient.get(url)
  }

  changPassword(form: any) {
    const url = `${environment.mainUrl}/account/password-reset-successful`
    return this.httpClient.patch(url, form)
  }

  resetPassword(form) {
    const url = `${environment.mainUrl}/account/request-password`
    return this.httpClient.post(url, form)
  }

  uploadPhotos(form: any) {
    const url = `${environment.mainUrl}/school-detail/create-gallery`
    return this.httpClient.post(url, form)
  }

  uploadVideos(form: any) {
    const url = `${environment.mainUrl}/school-detail/create-video`
    return this.httpClient.post(url, form)
  }

  /**
 * handleErr(err: HttpErrorResponse) {
  let msg = ''
  if (err.error instanceof ErrorEvent) {
    msg = err.message
  } else {
    msg = `Err was caused by ${err.status}\ ${err.message}`
  }
  return throwError(msg)
}
 */
}
