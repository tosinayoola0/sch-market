import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../service/auth.service';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})


export class ChangePasswordComponent implements OnInit {
  id;

  constructor(private authService: AuthService) { }

  changePswdForm = new FormGroup({
    old_password: new FormControl(['', Validators.required]),
    new_password: new FormControl(['', Validators.required]),
    confirm_password: new FormControl(['', Validators.required]),

  })

  changePswrd = {
    old_password: '',
    new_password: '',
    confirm_password: ''
  }

  ngOnInit(): void {
    this.id = localStorage.getItem('id')
  }


  onSubmit(form) {
    this.authService.changePassword(this.id, form).subscribe(
      (res) => {
        const msg = 'password successfully Updated'
        this.authService.resultHandler(msg)
      },
      err => this.authService.errorHandler(err)

    )
    console.log(form)
  }

}
