import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './../service/auth.service'
import { UserDetail } from './../data/user-detail';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.signUpForm = this.fb.group({
      email: [
        '', [Validators.required]
      ],
      password: [
        '', [Validators.required]
      ],
      username: [
        '', [Validators.required]
      ]
    })
  }


  signUp() {
    this.authService.userSignup(this.signUpForm.value).subscribe((res) => {
      if (res) {
        console.log('signup successful');
        this.authService.resultHandler(res)
        this.router.navigate(['/account/login'])
      }
    },
      (err) => {
        this.authService.errorHandler(err)
        console.log(err)
      }
    )

  }
}
