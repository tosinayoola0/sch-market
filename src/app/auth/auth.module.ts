import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { ChangePasswordComponent } from './change-password/change-password.component'
import { HomeModule } from '../home/home.module';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const Components = [
  SignupComponent,
  LoginComponent,
  ChangePasswordComponent
]
@NgModule({
  declarations: [Components, UpdatePasswordComponent, ResetPasswordComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    HomeModule
  ],
  exports: [Components]

})
export class AuthModule { }
