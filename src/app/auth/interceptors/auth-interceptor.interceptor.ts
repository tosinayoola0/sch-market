
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { environment } from '../../../environments/environment'

@Injectable()
export class AuthInterceptorInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token')
    //const authToken = this.authService.getToken()
    if (token) {
      //if (!request.headers.has('Content-Type')) {
      request = request.clone({
        setHeaders: {
          // 'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      });

      //}
    }
    return next.handle(request);
  }
}
