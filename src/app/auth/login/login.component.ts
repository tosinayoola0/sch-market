import { Component, OnInit } from '@angular/core';
import { Login } from './../data/login'
import { FormBuilder, FormGroup, Validator, EmailValidator, Validators } from '@angular/forms'
import { AuthService } from '../service/auth.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup

  constructor(
    private authService: AuthService,
    private router: Router,
    private fB: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.userLogin
    this.loginForm = this.fB.group({
      email: [
        '', [Validators.required]
      ],
      password: [
        '', [Validators.required]
      ]
    })
  }
  userLogin() {
    this.authService.login(this.loginForm.value)
  }

}
