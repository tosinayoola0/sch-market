import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  pswd_token
  reset_uidb64
  changePasswordForm;
  constructor(
    private authService: AuthService,
    private aRoute: ActivatedRoute,
    private route: Router,
  ) { }
  token = this.aRoute.snapshot.paramMap.get('token')
  uidb64 = this.aRoute.snapshot.paramMap.get('uidb64')
  ngOnInit(): void {

    this.changePasswordForm = new FormGroup({
      password: new FormControl('', Validators.required),
      confirm_password: new FormControl('', Validators.required),
      token: new FormControl('', Validators.required),
      uidb64: new FormControl('', Validators.required)
    })

    this.authService.getPasswordToken(this.token, this.uidb64).subscribe((res: any) => {
      console.log(res)
      this.pswd_token = res.token
      this.reset_uidb64 = res.uidb64
    })

  }




  onSubmit() {
    this.changePasswordForm.controls['token'].setValue(this.pswd_token)
    this.changePasswordForm.controls['uidb64'].setValue(this.reset_uidb64)
    console.log(this.changePasswordForm.value)
    this.authService.changPassword(this.changePasswordForm.value).subscribe(
      res => {
        this.authService.resultHandler(res)
        this.route.navigate(['/login'])
      },
      err => this.authService.errorHandler(err)
    )
  }






}


