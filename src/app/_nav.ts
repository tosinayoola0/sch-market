import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/school-dashboard',
    icon: 'icon-user',

  },
  {
    name: 'School Profile',
    url: '/school/profile',
    icon: 'icon-user',

  },
  {
    name: 'Principal Detail',
    url: `/school/principal-detail`,
    icon: 'icon-upload',

  },
  {
    name: 'Select Plan',
    url: '/school/subscription-page',
    icon: 'icon-money',

  },

  {
    name: 'Upload School Video',
    url: '/school/upload-video',
    icon: 'icon-upload',

  },
  {
    name: 'Available Plans',
    url: '/school/subscription-page',
    icon: 'icon-calculator',
  },
  {
    name: 'Upload School Gallery',
    url: '/school/sch-gallery',
    icon: 'icon-calculator',
  },

  {
    name: 'Change Password',
    url: '/account/change-password',
    icon: 'icon-delete',
  },

  {
    name: 'logout',
    url: '/school/logout',
    icon: 'icon-logout',

  }
]



